<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Template</title>
    <link rel="icon" type="icon/png" href="">
    <link rel="stylesheet" type="text/css" href="temp.css">
    
    <style>
       
        header{
            position: relative;
            margin: auto;/*Margen automatico*/
            text-align: center;/*Alinieando*/
            padding: 5px;/*margen interior*/ 
        }
        nav{
            position: relative;
            margin: auto;
            width: 100%;/*ancho*/ 
            height: auto;/*alto*/
            background: black;/*Color de fondo*/
        }
        nav ul{
            position: relative;
            margin: auto;
            width: 50%;
            text-align: center;
            color: white;
        }

        nav ul li{
            display: inline-block;
            width: 24%;
            line-height: 50px;
            list-style: none;
        }

        nav ul li a{
            color: white;
            text-decoration: none;
        }
        section{
            position:relative;
            padding: 20px;
        }

    </style>
</head>
<body>
    <header>
        <h1>LOGOTIPO</h1>
    </header>
<?php
include "etiquetas/navegacion.php";
?>
<section>
<?php

$mvc = new MvcController();
$mvc -> enlacesPaginasController();

?>
</section>

</body>
</html>
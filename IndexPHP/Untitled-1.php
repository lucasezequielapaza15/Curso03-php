
<?php

#variable numerica 
$numero = 5;
echo "Esto es una variable numero: $numero";
echo "<br><br>";
var_dump($numero);
echo "<br><br>";

#variable texto
$palabra= "palabra";
echo "Esto es una variable texto: $palabra";
echo "<br><br>";
var_dump($palabra);
echo "<br><br>";

#variable boleana
$boleana = true;
echo "Esto es una variable boleana: $boleana";
echo "<br><br>";
var_dump($boleana);
echo "<br><br>"; 

#variable arreglo 
$colores = array("rojo", "amarillo");
echo "Esto es una variable arreglo: $colores[1]";
echo "<br><br>";
var_dump($colores);
echo "<br><br>"; 

#variable arreglo con propiedades 
$verduras  = array("verdura1"=>"lechuga","verdura2"=>"cebolla");
echo "Esto es una variable arreglo con propiedades: $verduras[verdura1]";
echo "<br><br>";
var_dump($verduras);
echo "<br><br>";

#variable objeto

$frutas = (object) ["fruta1"=>"pera","fruta2"=>"manzana"];
echo "Esto es una variable objeto: $frutas->fruta1";
echo "<br><br>";
var_dump($frutas);

?>




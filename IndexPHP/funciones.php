<?php

#funciones sin parametros

 function saludo(){
     echo "hola<br>";
 }

 saludo();


#funciones con parametros
function despedida($adios){
    echo $adios."<br>";
}

despedida("chao"); #<-- puedo reemplazar la palabra

#funciones con retorno

function retorno ($retornar){
    return $retornar;

}

echo retorno("retornar");#<-- puedo reemplazar la palabra

?>